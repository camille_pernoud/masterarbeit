# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : It is the repository for the work i'm going to do in my MasterArbeit about concurrent data streaming in CUDA

* [Wiki's home:](https://bitbucket.org/camille_pernoud/masterarbeit/wiki/Home)
* [Bibliography:](https://bitbucket.org/camille_pernoud/masterarbeit/wiki/Bibliography)
* [Log:](https://bitbucket.org/camille_pernoud/masterarbeit/wiki/Log)
* Version

### Who do I talk to? ###

* Camille Pernoud <camille.pernoud@insa-lyon.fr>
* Johannes Jurgovsky <johannes.jurgovsky@insa-lyon.fr>

### Deadlines

* Monday 9th May : Begining talk
* Monday 31st August : First report
* Monday 12th September : Second report
* Wednesday 21th September : End talk
* Friday 30th September : Final report

### Topic
**The background is the following:**

- For many real-world tasks (speech recognition, cruise control, etc) it is essential to build a model of the signals of interest and the environment.
- Most tasks are too complex for humans to formalize them, thus we use learning algorithms that are capable of building this model for us.
- In order to build accurate models, learning algorithms require lots of data to learn from.
- Many of these learning algorithms can be extensively parallelized and therefore lead to significant speedups when implemented on GPUs.
<br>
<br>

**Work in the context of Uni Passau and Worldline:**

- For data analysis in the fraud detection scenario, we make extensive use of learning algorithms running on NVIDIA CUDA GPUs.
- Usually, we are not programming directly on CUDA devices but we use libraries that provide abstractions from the inner workings of GPUs.
- In the future we will be dealing with datasets that are too large to fit into the GPUs memory.
<br>
<br>

**Problem setting:**

- Data transfers and memory allocation can be very costly (w.r.t. time) compared to the actual computation
- Therefore, we are interested in a streaming-solution that loads data from the hard drive while at the same time the GPU computes some function
- As function we use one particular learning algorithm (but it can be any other algorithm). It is called repeatedly to do some computation on a chunk of data that is given as input.
- During the computation of this function on the GPU, the CPU should not be idle but instead load the next data-chunk from HDD to RAM and transfer it to the RAM of the GPU.
<br>
<br>

**Work:**

- Implement a native C/CUDA streaming mechanism to efficiently transfer data from HDD to the GPU’s RAM without interrupting the function execution
- Implement a Python-based streaming mechanism coupled to the Python libraries Theano and Tensorflow
- Evaluate and compare the efficiency of the native solution with the library-based solutions under slight variations of the problem setting and investigate the limitations in one or the other.
<br>
<br>

**Workplan:**

- Familiarize yourself with C Programming, the CUDA architecture and the CUDA Toolkit
- Familiarize yourself with concurrency and the requirements in this problem setting
- Implement a streaming solution by using mechanisms from the CUDA API
- Familiarize yourself with Theano and Tensorflow
- Identify the limitations imposed by the libraries on achieving a similarly efficient data / computation concurrency
- Implement the most efficient solutions
- Evaluate and compare these solutions
 

### Questions for Johannes ###
* What type of hardware ? -> **Single GPU-Workstation at UniPa [Tesla K80](http://www.nvidia.com/object/tesla-k80.html). You'll get an account soon.**
* What hardward ? Worldline's ? Mine ? -> **We only have one GPU in the workstation right now, but with two cores.**
* What type of target architecture ? One or multi-GPU ? **2 cores** What GPU family ? Fermi architecture or an other one ? What are the specifications of the hardware ? How many concurrent stream ? -> **Once you have your account, you can query the device for its properties.**

![teslaK80.png](https://bitbucket.org/repo/BEzLrb/images/3676236520-teslaK80.png)

* How the streamind data process work now ? I remember it deals with chunks of data but are the data from the (N)th chunk linked to the data data from the (N+1)th chunk ?  **All chunks are independants**
* Example of the algorithm used now ? -> **Word2Vec Skip-Gram algorithm for learning a language model. See a CPU-implementation in the python library [Gensim](https://radimrehurek.com/gensim/).** 
* What data ? -> **Plain text data from [Wikipedia articles](https://dumps.wikimedia.org/)**


---

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines