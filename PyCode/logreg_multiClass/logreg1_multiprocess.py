__author__ = 'johannesjurgovsky', 'camillepernoud'

import timeit
from multiprocessing import Pool, Manager, Process
import os
from struct import unpack

from GpuDispatcher import GpuDispatcher
from Preprocessing import Preprocessing
from Reader import Reader


def get_info(imagefile, labelfile):
    """Read input-vector (image) and target class (label, 0-9) and return
       it as list of tuples.
    """
    # Open the images with gzip in read binary mode
    # images = gzip.open(imagefile, 'rb')
    # labels = gzip.open(labelfile, 'rb')

    lab_start_time = timeit.default_timer()

    # Camille : No gzip in our use case
    images = open(imagefile, 'rb')
    labels = open(labelfile, 'rb')

    # Read the binary data

    # We have to get big endian unsigned int. So we need '>I'

    # Get metadata for images
    images.read(4)  # skip the magic_number
    number_of_images = images.read(4)
    number_of_images = unpack('>I', number_of_images)[0]
    rows = images.read(4)
    rows = unpack('>I', rows)[0]
    cols = images.read(4)
    cols = unpack('>I', cols)[0]

    # Get metadata for labels
    labels.read(4)  # skip the magic_number
    N = labels.read(4)
    N = unpack('>I', N)[0]

    if number_of_images != N:
        raise Exception('number of labels did not match the number of images')

    # 2D dimension
    rowscols = rows * cols

    lab_end_time = timeit.default_timer()
    print ('Get info ran for %.1fs' % ((lab_end_time - lab_start_time)))
    return number_of_images, rowscols

def consumer((fct, arg)):
    if fct == "load_data_1a":
        print ("load_data_1a")
        reader = Reader(arg[0], arg[1], arg[2])
        N = arg[3]
        rowscols = arg[4]
        read_start_time = timeit.default_timer()
        for i in range(0, N, 2):
            reader.read(i, rowscols)
        read_end_time = timeit.default_timer()
        print ('Read ran for %.1fs' % ((read_end_time - read_start_time)))
    else:
        if fct == "load_data_1b":
            print ("load_data_1b")
            reader = Reader(arg[0], arg[1], arg[2])
            N = arg[3]
            rowscols = arg[4]
            read_start_time = timeit.default_timer()
            for i in range(1, N, 2):
                reader.read(i, rowscols)
            read_end_time = timeit.default_timer()
            print ('Read ran for %.1fs' % ((read_end_time - read_start_time)))
        else:
            if fct == "preproc":
                print ("preproc bla ")
                preproc = Preprocessing(arg[0], arg[1], arg[2])
                preproc.preproc()
            else:
                if fct == "compute":
                    print ("compute")
                    # compute(arg)
                    compute = GpuDispatcher(arg[0])
                    compute.compute()
                else:
                    print ("Fonction error")


if __name__ == "__main__":
    print ("Start time recording")
    tot_start_time = timeit.default_timer()

    nb_process_read = 3
    nb_process_preprocess = 1
    nb_process_compute = 3

    i = 0
    for dirname, dirnames, filenames in os.walk('./new_mnist/train'):
        for filename in filenames:
            i = i + 1
    nb_files = i / 2

    i = 0
    for dirname, dirnames, filenames in os.walk('./new_mnist/test'):
        for filename in filenames:
            i = i + 1
    nb_files_test = i / 2

    if nb_files != nb_files_test:
        raise Exception('number of files is not the same')

    imagefile = "new_mnist/train/0_patterns"
    labelfile = "new_mnist/train/0_labels"
    nb_example_files, rowscols = get_info(imagefile, labelfile)
    for i in range(1, nb_files):
        imagefile = "new_mnist/train/%d_patterns" % i
        labelfile = "new_mnist/train/%d_labels" % i
        N, rowscols = get_info(imagefile, labelfile)
        if (i > 1):
            if nb_example_files != N:
                raise Exception('number of images is not the same in every file - train file %i' %i)

    imagefile = "new_mnist/test/0_patterns"
    labelfile = "new_mnist/test/0_labels"
    nb_example_files, rowscols = get_info(imagefile, labelfile)
    for i in range(1, nb_files):
        imagefile = "new_mnist/test/%d_patterns" % i
        labelfile = "new_mnist/test/%d_labels" % i
        N, rowscols = get_info(imagefile, labelfile)
        if (i > 1):
            if nb_example_files != N:
                raise Exception('number of images is not the same in every file - test file %i' % i)

    # nb_example_files = 50000  # 50 000

    m = Manager()

    q_train = m.Queue()
    q_input = m.Queue()

    pool_read = Pool(nb_process_read)

    print ("queue input {}".format(q_input.qsize()))
    print ("queue train {}".format(q_train.qsize()))
    data = []

    # test = nb_example_files / 10
    for i in range(nb_files):
        data.append(["load_data_1a",
                     ["new_mnist/train/%d_patterns" % i, "new_mnist/train/%d_labels" % i, q_input, nb_example_files,
                      rowscols]])
        data.append(["load_data_1b",
                     ["new_mnist/train/%d_patterns" % i, "new_mnist/train/%d_labels" % i, q_input, nb_example_files,
                      rowscols]])
        # data.append(["load_data_1a",
        #              ["new_mnist/test/%d_patterns" % i, "new_mnist/test/%d_labels" % i, q_input, nb_example_files,
        #               rowscols]])
        # data.append(["load_data_1b",
        #              ["new_mnist/test/%d_patterns" % i, "new_mnist/test/%d_labels" % i, q_input, nb_example_files,
        #               rowscols]])

    tab = pool_read.map(consumer, data)

    print ("queue input {}".format(q_input.qsize()))
    print ("queue train {}".format(q_train.qsize()))

    data = []
    pool_preproc = Pool(nb_process_preprocess)

    for i in range(nb_files):
        print ('add preproc')
        data.append(["preproc", [q_input, q_train, nb_example_files]])

    pool_preproc.map(consumer, data)

    print ("queue input {}".format(q_input.qsize()))
    print ("queue train {}".format(q_train.qsize()))

    # print ("TEST DATA ----------------------------------------")
    data = []
    pool_read2 = Pool(nb_process_preprocess)

    for i in range(nb_files):
        data.append(["load_data_1a",
                     ["new_mnist/test/%d_patterns" % i, "new_mnist/test/%d_labels" % i, q_input, nb_example_files,
                      rowscols]])
        data.append(["load_data_1b",
                     ["new_mnist/test/%d_patterns" % i, "new_mnist/test/%d_labels" % i, q_input, nb_example_files,
                      rowscols]])

    tab = pool_read.map(consumer, data)

    data = []
    pool_preproc2 = Pool(nb_process_preprocess)
    for i in range(nb_files):
        data.append(["preproc", [q_input, q_train, nb_example_files]])

    pool_preproc2.map(consumer, data)

    print ("queue input {}".format(q_input.qsize()))
    print ("queue train {}".format(q_train.qsize()))

    # data = []
    # pool = Pool(nb_process_compute)
    #
    # for i in range(nb_files):
    #     data.append(["compute", [q_train]])
    #
    # pool.map(consumer, data)

    processes = []
    for i in range(nb_files):
        p = Process(target=consumer, args=([["compute", [q_train]]]))
        p.daemon = False
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    print ("queue input {}".format(q_input.qsize()))
    print ("queue train {}".format(q_train.qsize()))

    # try:
    #     pool.terminate()
    # except WindowsError:
    #     pass

    tot_end_time = timeit.default_timer()
    print ('Global application ran for %.1fs' % ((tot_end_time - tot_start_time)))
