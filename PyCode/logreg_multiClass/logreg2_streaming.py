__author__ = 'johannesjurgovsky', 'camillepernoud'

import timeit
import argparse
import sys
from multiprocessing import Pool, Manager, Process
import os
import time
from struct import unpack
import csv

from GpuDispatcher import GpuDispatcher
from Preprocessing import Preprocessing
from Reader import Reader


# from Logger import create_logger

# global logger
# logger = create_logger('lab.log')

def sync(compute1, compute2, sync_time):
    while 1:
        compute1.sync(compute2)
        time.sleep(sync_time)


def get_info(imagefile, labelfile):
    """Read input-vector (image) and target class (label, 0-9) and return
       it as list of tuples.
    """
    # Open the images with gzip in read binary mode
    # images = gzip.open(imagefile, 'rb')
    # labels = gzip.open(labelfile, 'rb')

    lab_start_time = timeit.default_timer()

    # Camille : No gzip in our use case
    images = open(imagefile, 'rb')
    labels = open(labelfile, 'rb')

    # Read the binary data

    # We have to get big endian unsigned int. So we need '>I'

    # Get metadata for images
    images.read(4)  # skip the magic_number
    number_of_images = images.read(4)
    number_of_images = unpack('>I', number_of_images)[0]
    rows = images.read(4)
    rows = unpack('>I', rows)[0]
    cols = images.read(4)
    cols = unpack('>I', cols)[0]

    # Get metadata for labels
    labels.read(4)  # skip the magic_number
    N = labels.read(4)
    N = unpack('>I', N)[0]

    if number_of_images != N:
        raise Exception('number of labels did not match the number of images')

    # 2D dimension
    rowscols = rows * cols

    lab_end_time = timeit.default_timer()
    # logger.info('Get info ran for %.1fs' % ((lab_end_time - lab_start_time)))
    return number_of_images, rowscols


def init_reader_train(nb_process_read, nb_example_files, max_qsize, rowscols, nb_files, q_input):
    pool_read = Pool(nb_process_read)
    data = []
    to_read_a = []
    to_read_b = []
    for i in range(0, nb_example_files, 2):
        to_read_a.append(i)
    for i in range(1, nb_example_files, 2):
        to_read_b.append(i)
    for i in range(nb_files):
        data.append(["load_data",
                     ["new_mnist/train/img/%d_patterns" % i, "new_mnist/train/lab/%d_labels" % i, q_input, max_qsize,
                      rowscols, to_read_a]])
        data.append(["load_data",
                     ["new_mnist/train/img/%d_patterns" % i, "new_mnist/train/lab/%d_labels" % i, q_input, max_qsize,
                      rowscols, to_read_b]])

    pool_read.map(consumer, data)
    q_input.put(['DUMMY', 'DUMMY'])


def init_reader_test(nb_process_read, nb_example_files, q_time, rowscols, nb_files, q_input):
    pool_read = Pool(nb_process_read)
    data = []
    to_read_a = []
    for i in range(nb_example_files):
        to_read_a.append(i)

    for i in range(nb_files):
        data.append(["load_data",
                     ["new_mnist/test/img/%d_patterns" % i, "new_mnist/test/lab/%d_labels" % i, q_input,
                      q_time,
                      rowscols, to_read_a]])

    pool_read.map(consumer, data)
    q_input.put(['DUMMY', 'DUMMY'])


def init_preproc(nb_process_preprocess, batch_size, q_input, q_train, q_time, nb_gpu):
    data = []
    pool_preproc = Pool(nb_process_preprocess)

    data.append(["preproc", [q_input, q_train, q_time, batch_size]])

    pool_preproc.map(consumer, data)
    for i in range(nb_gpu):
        q_train.put(['DUMMY', 'DUMMY'])


def consumer((fct, arg)):
    if fct == "load_data":
        read_start_time = timeit.default_timer()
        reader = Reader(arg[0], arg[1], arg[2], arg[3], arg[4], arg[5])
        # Reader(imagefile, labelfile, queue_in, q_time, rowscols, list_indices)
        reader.start()
        read_end_time = timeit.default_timer()
        # print ('Read ran for %.1fs' % ((read_end_time - read_start_time)))
    else:
        if fct == "preproc":
            preproc = Preprocessing(arg[0], arg[1], arg[2], arg[3])
            preproc.preproc()
        else:
            print ("Fonction error")


def init_train(compute):
    compute.train()


def launch_queue_train(compute):
    compute.q_order.put("train")
    res = compute.q_order.get()
    while res != "train_ok":
        compute.q_order.put(res)
        res = compute.q_order.get()


def main(args):
    print ("Start time recording")
    tot_start_time = timeit.default_timer()

    parser = argparse.ArgumentParser(description="Do something.")
    parser.add_argument("-r", "--reader", type=int, default=1, required=False)
    parser.add_argument("-p", "--preprocessor", type=int, default=1, required=False)
    parser.add_argument("-b", "--batch_size", type=int, default=500, required=False)
    parser.add_argument("-q", "--queue_size", type=int, default=20, required=False)
    parser.add_argument("-g", "--gpu", type=int, default=1, required=False)
    args = parser.parse_args(args)

    # logger = create_logger('main.log')

    nb_process_read = args.reader
    nb_process_preprocess = args.preprocessor
    batch_size = args.batch_size
    max_qsize = args.queue_size  # 1 000
    sync_time = 10  # seconds # sleep while sync
    nb_gpu = args.gpu  # 1 or 2
    gpu0 = 'gpu0'
    gpu1 = 'gpu1'

    print ("nb process read : %i" % nb_process_read)
    print("nb process preprocessing  : %i" % nb_process_preprocess)
    print("batch size : %i" % batch_size)
    print ("max queue size : %i" % max_qsize)
    print("time between 2 sync : %i" % sync_time)
    print ("nb gpu : %i" % nb_gpu)

    i = 0
    for dirname, dirnames, filenames in os.walk('./new_mnist/train'):
        for filename in filenames:
            i = i + 1
    nb_files = i / 2

    i = 0
    for dirname, dirnames, filenames in os.walk('./new_mnist/test'):
        for filename in filenames:
            i = i + 1
    nb_files_test = i / 2

    if nb_files != nb_files_test:
        raise Exception('number of files is not the same')

    imagefile = "new_mnist/train/img/0_patterns"
    labelfile = "new_mnist/train/lab/0_labels"
    nb_example_files, rowscols = get_info(imagefile, labelfile)
    for i in range(1, nb_files):
        imagefile = "new_mnist/train/img/%d_patterns" % i
        labelfile = "new_mnist/train/lab/%d_labels" % i
        N, rowscols = get_info(imagefile, labelfile)
        if (i > 1):
            if nb_example_files != N:
                raise Exception('number of images is not the same in every file - train file %i' % i)

    imagefile = "new_mnist/test/img/0_patterns"
    labelfile = "new_mnist/test/lab/0_labels"
    nb_example_files, rowscols = get_info(imagefile, labelfile)
    for i in range(1, nb_files):
        imagefile = "new_mnist/test/img/%d_patterns" % i
        labelfile = "new_mnist/test/lab/%d_labels" % i
        N, rowscols = get_info(imagefile, labelfile)
        if (i > 1):
            if nb_example_files != N:
                raise Exception('number of images is not the same in every file - test file %i' % i)

    m = Manager()

    print ("MANAGER")

    q_train = m.Queue(max_qsize)
    q_input = m.Queue(max_qsize)
    q_read_time = m.Queue()
    q_preproc_time = m.Queue()
    q_gpu_time = m.Queue()
    q_sync_time = m.Queue()

    l_compute = m.list()

    print ("QUEUE")

    compute = GpuDispatcher(q_train, gpu0, q_gpu_time, q_sync_time)
    p3 = Process(target=compute.run,
                 args=())
    p3.daemon = False
    p3.start()

    if nb_gpu == 2:
        compute2 = GpuDispatcher(q_train, gpu1, q_gpu_time, q_sync_time, compute)
        p4 = Process(target=compute2.run,
                     args=())
        p4.daemon = False
        p4.start()

    # while(hasattr(compute, 'classifier') != True):
    #     print (hasattr(compute, 'classifier'))
    #     print ("WAIT CLASSIFIER")
    #     time.sleep(10)

    print ("GPU DISPATCHER CREATED")
    # print ("queue input {}".format(q_input.qsize()))
    # print ("queue train {}".format(q_train.qsize()))

    epoch_start_time = timeit.default_timer()

    p = Process(target=init_reader_train,
                args=([nb_process_read, nb_example_files, q_read_time, rowscols, nb_files, q_input]))
    p.daemon = False
    p.start()

    print ("READER CREATED")

    # print ("queue input {}".format(q_input.qsize()))
    # print ("queue train {}".format(q_train.qsize()))

    p2 = Process(target=init_preproc,
                 args=([nb_process_preprocess, batch_size, q_input, q_train, q_preproc_time, nb_gpu]))
    p2.daemon = False
    p2.start()

    print ("PREPROCESSOR CREATED")

    # print ("queue input {}".format(q_input.qsize()))
    # print ("queue train {}".format(q_train.qsize()))

    # compute.train()
    # compute2.train()

    # p3 = Process(target=init_train,
    #              args=([compute]))
    # p3.daemon = True
    # p3.start()
    #
    # p3.join()

    p5 = Process(target=launch_queue_train, args=([compute]))
    p5.daemon = False
    p5.start()

    if nb_gpu == 2:
        p6 = Process(target=launch_queue_train, args=([compute2]))
        p6.daemon = False
        p6.start()

    p5.join()
    if nb_gpu == 2:
        p6.join()
        # p_sync = Process(target=sync, args=([compute, compute2, sync_time]))
        # p_sync.daemon = False
        # p_sync.start()

    print ("PROCESS JOINED")

    e_err = 0.
    nb = 0.
    # logger.info ("begin compute")
    for i in range(compute.q.qsize()):
        err = compute.q.get()
        # print ("get done %i" %i)
        e_err += err[0]
        nb += err[1]
    if nb_gpu == 2:
        for i in range(compute2.q.qsize()):
            err = compute2.q.get()
            # print ("get done %i" %i)
            e_err += err[0]
            nb += err[1]
    p.join()
    p2.join()
    print("Negative Log-Likelihood (mean over training set): %.6f / Err : %i, examples processed : %i" % (
    e_err / nb, e_err, nb))
    epoch_end_time = timeit.default_timer()
    print ('Epoch global time %i : %.2f examples per sec' % (
        (epoch_end_time - epoch_start_time), nb / (epoch_end_time - epoch_start_time)))

    # print ("queue input {}".format(q_input.qsize()))
    # print ("queue train {}".format(q_train.qsize()))
    # print ("Both should be 0")

    if nb_gpu == 2:
        # p_sync.terminate()
        # compute.sync(compute2)
        compute2.q_order.put("stop")
    eval_start_time = timeit.default_timer()

    p = Process(target=init_reader_test,
                args=([nb_process_read, nb_example_files, q_read_time, rowscols, nb_files, q_input]))
    p.daemon = False
    p.start()

    p2 = Process(target=init_preproc, args=([nb_process_preprocess, batch_size, q_input, q_train, q_preproc_time, 1]))
    p2.daemon = False
    p2.start()

    # print ("queue input {}".format(q_input.qsize()))
    # print ("queue train {}".format(q_train.qsize()))

    # logger.info ("begin eval")
    # compute.test()

    compute.q_order.put("test")
    res = compute.q_order.get()
    while res != "test_ok":
        compute.q_order.put(res)
        res = compute.q_order.get()

    # p3 = Process(target=compute.test,
    #              args=())
    # p3.daemon = False
    # p3.start()
    #
    # p3.join()

    e_mistakes = 0.
    nbRes = 0.
    for i in range(compute.q_res.qsize()):
        loc_mistake = compute.q_res.get()
        e_mistakes += loc_mistake[0]
        nbRes += loc_mistake[1]
    compute.q_order.put("stop")

    print ("0/1 Loss on Testset (Number of Mistakes): %d / %d : %.6f" % (e_mistakes, nbRes, e_mistakes / nbRes))

    p.join()
    p2.join()
    # p3.join()

    # logger.info ("queue 1 {}".format(q_input.qsize()))
    # logger.info ("queue 2 {}".format(q_train.qsize()))
    # logger.info ("queue 3 {}".format(compute.q.qsize()))
    # logger.info ("queue 4 {}".format(compute.q_res.qsize()))

    eval_end_time = timeit.default_timer()
    print ('Eval global time %i : %.2f examples per sec' % (
    (eval_end_time - eval_start_time), nbRes / (eval_end_time - eval_start_time)))

    tot_end_time = timeit.default_timer()
    print ('Global application ran for %.1fs' % ((tot_end_time - tot_start_time)))

    read_time = 0.
    for i in range(q_read_time.qsize()):
        read_time = read_time + q_read_time.get()
    print ("Total effective reading time : %.2f" % read_time)

    preproc_time = 0.
    for i in range(q_preproc_time.qsize()):
        preproc_time = preproc_time + q_preproc_time.get()
    print ("Total effective preproc time : %.2f" % preproc_time)

    gpu_time = 0.
    for i in range(q_gpu_time.qsize()):
        gpu_time = gpu_time + q_gpu_time.get()
    print ("Total effective GPU time : %.2f" % gpu_time)

    sync_time = 0.
    for i in range(q_sync_time.qsize()):
        sync_time = sync_time + q_sync_time.get()
    print ("Total effective sync time : %.2f" % sync_time)

    file = open('results.csv', 'a')
    writer = csv.writer(file)
    data = [[nb_process_read, nb_process_preprocess, batch_size, max_qsize, nb_gpu,
             nb / (epoch_end_time - epoch_start_time), nbRes / (eval_end_time - eval_start_time),
             (tot_end_time - tot_start_time), read_time, preproc_time, gpu_time, sync_time]]
    writer.writerows(data)
    file.close()


if __name__ == "__main__":
    main(sys.argv[1:])
