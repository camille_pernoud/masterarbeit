__author__ = 'johannesjurgovsky', 'camillepernoud'

import numpy as np
import timeit

class WEIGHT_INITIALIZATION(object):
    @staticmethod
    def getUniformWeights(shape, scale=1.):
        return scale * np.random.uniform(-1., 1., size=shape).astype(np.float32)

    @staticmethod
    def getNormalWeights(shape, mu, scale=1.):
        return np.random.normal(mu, scale, size=shape).astype(np.float32)


class LogisticRegression(object):
    def __init__(self, n_features, n_classes, custom_gpu, learning_rate=0.1):
        self.custom_gpu = custom_gpu
        import theano.sandbox.cuda
        theano.sandbox.cuda.use(self.custom_gpu)
        import theano
        import theano.tensor as T

        scale = np.sqrt(6. / (n_features + n_classes))

        nW = WEIGHT_INITIALIZATION().getUniformWeights((n_features, n_classes), scale)
        nb = np.zeros((n_classes,), dtype=theano.config.floatX)

        self.W = theano.shared(nW, name="W", borrow=True)
        self.b = theano.shared(nb, name="b")

        self.batch_x = T.fmatrix('x')
        self.batch_y = T.ivector('y')

        self.learning_rate = learning_rate
        self.p_y_given_x = T.nnet.softmax(T.dot(self.batch_x, self.W) + self.b)
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)
        self.params = [self.W, self.b]

        loss = -T.mean(T.log(self.p_y_given_x)[T.arange(self.batch_y.shape[0]), self.batch_y])
        gparams = T.grad(loss, self.params)

        updates = []
        for p, gp in zip(self.params, gparams):
            updates.append((p, p - learning_rate * gp))

        eval_01loss = T.sum(T.neq(self.y_pred, self.batch_y))

        self.train_fnc = theano.function([self.batch_x, self.batch_y], outputs=loss, updates=updates)
        self.predict_fnc = theano.function([self.batch_x], outputs=self.y_pred)
        self.eval_fnc = theano.function([self.batch_x, self.batch_y], outputs=eval_01loss)

    # def _compile(self, learning_rate=0.1):
    #     # import theano.sandbox.cuda
    #     # theano.sandbox.cuda.use(self.custom_gpu)
    #     # import theano
    #     # import theano.tensor as T


        # Debug to understand theano function

        # theano.printing.debugprint(self.train_fnc)
        # theano.printing.pydotprint(self.train_fnc, outfile="pics/logreg_pydotprint_train_fnc.png",
        #                            var_with_name_simple=True)
        # theano.printing.debugprint(self.predict_fnc)
        # theano.printing.pydotprint(self.predict_fnc, outfile="pics/logreg_pydotprint_predict_fnc.png",
        #                            var_with_name_simple=True)
        # theano.printing.debugprint(self.eval_fnc)
        # theano.printing.pydotprint(self.eval_fnc, outfile="pics/logreg_pydotprint_eval_fnc.png",
        #                            var_with_name_simple=True)


    def train(self, arg):
        train_start_time = timeit.default_timer()
        args = arg[0]
        batch_x = args['train_x']
        batch_y = args['train_y']
        q = args['q']
        loss = self.train_fnc(batch_x, batch_y)
        q.put([loss, batch_x.shape[0]])
        train_end_time = timeit.default_timer()
        # print ('Train logreg ran for %.1fs' % ((train_end_time - train_start_time)))

    def predict(self, batch_x):
        predictions = self.predict_fnc(batch_x)
        return predictions

    def evaluate(self, arg):
        # print ("evaluate called")
        eval_start_time = timeit.default_timer()
        args = arg[0]
        batch_x = args['test_x']
        batch_y = args['test_y']
        q = args['q']
        n_predictionErrors = self.eval_fnc(batch_x, batch_y)
        q.put([n_predictionErrors, batch_x.shape[0]])
        eval_end_time = timeit.default_timer()
        # print ('Evaluate logreg ran for %.1fs' % ((eval_end_time - eval_start_time)))

