import timeit
from numpy import zeros
import numpy as np

class Preprocessing(object):
    def __init__(self, queue_in, queue_out, q_time, batch_size):
        self.queue_in = queue_in
        self.queue_out = queue_out
        self.batch_size = batch_size
        self.q_time= q_time

    def preproc(self):

        total_time = 0.
        x = zeros((self.batch_size, 784), dtype=np.float32)
        y = zeros((self.batch_size), dtype=np.float32)
        bexec = True
        i = 0
        j = 0
        while(bexec):
            # if j % 1000 == 0:
            #     print("preproc : %i" % j)
            if self.queue_in.empty():
                print ("READING QUEUE IS EMPTY")
            res = self.queue_in.get()
            j = j +1
            if res[0] == 'DUMMY':
                bexec = False
                print ("STOP PREPROC")
            else:
                preproc_start_time = timeit.default_timer()
                x[i] = res[0]
                y[i] = res[1]
                i = i +1
                preproc_end_time = timeit.default_timer()
                if i == self.batch_size:
                    std_x_float = ((x - np.mean(x)) / np.std(x)).astype(np.float32)
                    y_int = y.astype(np.int32)
                    preproc_end_time = timeit.default_timer()
                    if self.queue_out.full():
                        print ("PREPROCESSING QUEUE IS FULL")
                    self.queue_out.put([std_x_float, y_int])
                    # print ("queue read {}".format(self.queue_in.qsize()))
                    # print ("queue train {}".format(self.queue_out.qsize()))
                    i = 0
                    x = zeros((self.batch_size, 784), dtype=np.float32)
                    y = zeros((self.batch_size), dtype=np.float32)
                total_time = preproc_end_time - preproc_start_time
                self.q_time.put(total_time)
        # print ('Preproc ran for %.1fs' % ((preproc_end_time - preproc_start_time)))

