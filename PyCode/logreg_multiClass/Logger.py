def create_logger(filename):
    # Logging part from http://sametmax.com/ecrire-des-logs-en-python/
    # create logger
    import logging
    from logging.handlers import RotatingFileHandler

    # create logger object
    logger = logging.getLogger()
    # level
    logger.setLevel(logging.DEBUG)

    # File logger - formatter : add the time to each message
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    file_handler = RotatingFileHandler(filename, 'a', 1000000, 1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # Console handler
    steam_handler = logging.StreamHandler()
    steam_handler.setLevel(logging.DEBUG)
    logger.addHandler(steam_handler)
    return logger