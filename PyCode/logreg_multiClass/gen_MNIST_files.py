# Create MNIST files
#is working To intergrate is needed

import os

offset = 0

# Daily debug
files = 5 #5
examples_per_file = 5000 #50000

def remove_from_dir(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
        except Exception as e:
            print(e)

# Evaluation
# files = 1000
# examples_per_file_train = 100000 # 100 000 examples / files. 10 000 test examples.

# examples_per_file_test = examples_per_file_train * 0.1 # test dataset = 10% train dataset

if __name__ == "__main__":
    remove_from_dir('new_mnist/train')
    remove_from_dir('new_mnist/test')

    for i in range(files):
        print (i)
        strtIdx = offset + i * examples_per_file
        endIdx = offset + (i + 1) * examples_per_file - 1
        os.system("infimnist.exe pat %d %d > new_mnist/train/img/%d_patterns" % (strtIdx, endIdx, i))
        os.system("infimnist.exe lab %d %d > new_mnist/train/lab/%d_labels" % (strtIdx, endIdx, i))

    # To have independant dataset
    # offset = endIdx

    # examples_per_file = examples_per_file * 10

    for i in range(files):
        print (i)
        strtIdx = offset + i * examples_per_file
        endIdx = offset + (i + 1) * examples_per_file - 1
        os.system("infimnist.exe pat %d %d > new_mnist/test/img/%d_patterns" % (strtIdx, endIdx, i))
        os.system("infimnist.exe lab %d %d > new_mnist/test/lab/%d_labels" % (strtIdx, endIdx, i))