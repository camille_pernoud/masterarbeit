import timeit
from multiprocessing import Manager, Process
from LogisticRegression import LogisticRegression


class GpuDispatcher(object):
    def __init__(self, q_train, gpu, q_time, *args):
        self.q_train = q_train
        self.n_features = 784
        self.n_classes = 10
        self.gpu = gpu
        m = Manager()
        self.q = m.Queue()
        self.q_res = m.Queue()
        self.q_order = m.Queue()
        self.run_exec = True
        self.q_time = q_time
        self.q_sync_time = args[0]
        self.args = args

    def run(self):
        self.classifier = LogisticRegression(self.n_features, self.n_classes, self.gpu, learning_rate=0.01)
        print ("CLASSIFIER CREATED")
        while self.run_exec:
            order = self.q_order.get()
            if order == "train":
                self.train()
                # if self.args.__len__() == 2:
                #     self.sync(self.args[1])
                self.q_order.put("train_ok")
            else:
                if order == "test":
                    self.test()
                    self.q_order.put("test_ok")
                else:
                    if order == "stop":
                        self.run_exec = False
                        print ("STOP GPU RUN")
                        # self.q_order.put(order)

    def train(self):
        # Process.__init__(self)
        train_start_time = timeit.default_timer()
        bexec = True
        while bexec:
            if self.q_train.empty():
                print ("PREPROCESSING QUEUE IS EMPTY")
            res = self.q_train.get()
            if res[0] == 'DUMMY':
                bexec = False
                print ("STOP GPU TRAIN")
            else:
                m = Manager()
                train_x = res[0]
                train_y = res[1]
                args = m.list()
                args.append({})
                shared_args = args[0]
                shared_args['train_x'] = train_x
                shared_args['train_y'] = train_y
                shared_args['q'] = self.q
                args[0] = shared_args

                self.classifier.train(args)
        train_end_time = timeit.default_timer()
        self.q_time.put((train_end_time - train_start_time))
        # print ('Train data ran for %.1fs' % ((train_end_time - train_start_time)))

    def test(self):
        test_start_time = timeit.default_timer()
        # print ("test called")
        bexec = True
        while bexec:
            if self.q_train.empty():
                print ("PREPROCESSING QUEUE IS EMPTY")
            res = self.q_train.get()
            if res[0] == 'DUMMY':
                bexec = False
                print ("STOP GPU TEST")
            else:
                test_x = res[0]
                test_y = res[1]
                m = Manager()
                args = m.list()
                args.append({})
                shared_args = args[0]
                shared_args['test_x'] = test_x
                shared_args['test_y'] = test_y
                shared_args['q'] = self.q_res
                args[0] = shared_args
                self.classifier.evaluate(args)
        test_end_time = timeit.default_timer()
        self.q_time.put((test_end_time - test_start_time))
        # print ('Test data ran for %.1fs' % ((test_end_time - test_start_time)))

    def sync(self, logreg):
        sync_start_time = timeit.default_timer()
        a = self.classifier.W.get_value()
        b = logreg.classifier.W.get_value()
        mean = (a + b) /2
        self.classifier.W.set_value(mean, borrow=True)
        logreg.classifier.W.set_value(mean, borrow=True)
        sync_end_time = timeit.default_timer()
        self.q_sync_time.put((sync_end_time - sync_start_time))
        print ('Sync data ran for %.1fs' % ((sync_end_time - sync_start_time)))