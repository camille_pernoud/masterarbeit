import os
import csv

reader = [1, 2, 3, 4, 6, 8, 10, 12, 14]
# reader = [4]
preprocessor = [1, 2, 3, 4, 6, 8, 10, 12, 14]
# preprocessor = [1]
batch_size = [10, 100, 1000, 10000]
# batch_size = [1000]
queue_size = [10, 100, 1000, 10000]
# queue_size = [1000]
gpu = [1, 2]
file = open('results.csv', 'a')
writer = csv.writer(file)
data = [["nb_process_read", "nb_process_preprocess", "batch_size", "max_qsize", "nb_gpu",
         'ex per sec train', 'ex per sec test',
         'tot exec time', 'read_time', 'preproc_time', 'gpu_time', 'sync_time']]
writer.writerows(data)
file.close()
for i in range(len(reader)):
    for j in range(len(preprocessor)):
        for k in range(len(batch_size)):
            for l in range(len(queue_size)):
                for m in range(len(gpu)):
                    # print ("reader : %i"%reader[i])
                    # print ("preprocessor : %i" % preprocessor[j])
                    # print ("gpu : %i" %gpu[m])
                    totalProc = reader[i] + preprocessor[j] + gpu[m] + gpu[m] + 4 # 4 = autotest + logreg2 + init_reader + init_preproc || 2*gpu = dispatcher + launch_q_train
                    # print ("Total : %i" %totalProc)
                    if totalProc <= 16:
                        # print("EXEC")
                        os.system("python logreg2_streaming.py -r %i -p %i -b %i -q %i -g %i" % (
                        reader[i], preprocessor[j], batch_size[k], queue_size[l], gpu[m]))
                        # print ("python logreg2_streaming.py -r %i -p %i -b %i -q %i -g %i" % (
                        # reader[i], preprocessor[j], batch_size[k], queue_size[l], gpu[m]))
