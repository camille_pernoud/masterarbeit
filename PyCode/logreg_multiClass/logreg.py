__author__ = 'johannesjurgovsky', 'camillepernoud'

import timeit
from struct import unpack
import sys

import numpy as np
import theano
import theano.tensor as T
from numpy import zeros


class WEIGHT_INITIALIZATION(object):
    @staticmethod
    def getUniformWeights(shape, scale=1.):
        return scale * np.random.uniform(-1., 1., size=shape).astype(theano.config.floatX)

    @staticmethod
    def getNormalWeights(shape, mu, scale=1.):
        return np.random.normal(mu, scale, size=shape).astype(theano.config.floatX)


class LogisticRegression(object):
    def __init__(self, n_features, n_classes, learning_rate=0.1):
        scale = np.sqrt(6. / (n_features + n_classes))
        nW = WEIGHT_INITIALIZATION.getUniformWeights((n_features, n_classes), scale)
        nb = np.zeros((n_classes,), dtype=theano.config.floatX)

        self.W = theano.shared(nW, name="W", borrow=True)
        self.b = theano.shared(nb, name="b")

        self.batch_x = T.fmatrix('x')
        self.batch_y = T.ivector('y')

        self.learning_rate = learning_rate
        self._compile(learning_rate)

    def _compile(self, learning_rate=0.1):
        self.p_y_given_x = T.nnet.softmax(T.dot(self.batch_x, self.W) + self.b)
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)
        self.params = [self.W, self.b]

        loss = -T.mean(T.log(self.p_y_given_x)[T.arange(self.batch_y.shape[0]), self.batch_y])
        gparams = T.grad(loss, self.params)

        updates = []
        for p, gp in zip(self.params, gparams):
            updates.append((p, p - learning_rate * gp))

        eval_01loss = T.sum(T.neq(self.y_pred, self.batch_y))

        self.train_fnc = theano.function([self.batch_x, self.batch_y], outputs=loss, updates=updates)
        self.predict_fnc = theano.function([self.batch_x], outputs=self.y_pred)
        self.eval_fnc = theano.function([self.batch_x, self.batch_y], outputs=eval_01loss)

        # Debug to understand theano function

        # theano.printing.debugprint(self.train_fnc)
        # theano.printing.pydotprint(self.train_fnc, outfile="pics/logreg_pydotprint_train_fnc.png",
        #                            var_with_name_simple=True)
        # theano.printing.debugprint(self.predict_fnc)
        # theano.printing.pydotprint(self.predict_fnc, outfile="pics/logreg_pydotprint_predict_fnc.png",
        #                            var_with_name_simple=True)
        # theano.printing.debugprint(self.eval_fnc)
        # theano.printing.pydotprint(self.eval_fnc, outfile="pics/logreg_pydotprint_eval_fnc.png",
        #                            var_with_name_simple=True)

    def train(self, batch_x, batch_y):
        loss = self.train_fnc(batch_x, batch_y)
        return loss

    def predict(self, batch_x):
        predictions = self.predict_fnc(batch_x)
        return predictions

    def evaluate(self, batch_x, batch_y):
        n_predictionErrors = self.eval_fnc(batch_x, batch_y)
        return n_predictionErrors


def get_labeled_data(imagefile, labelfile):
    """Read input-vector (image) and target class (label, 0-9) and return
       it as list of tuples.
    """
    # Open the images with gzip in read binary mode
    # images = gzip.open(imagefile, 'rb')
    # labels = gzip.open(labelfile, 'rb')

    # Camille : No gzip in our use case
    images = open(imagefile, 'rb')
    labels = open(labelfile, 'rb')

    # Read the binary data

    # We have to get big endian unsigned int. So we need '>I'

    # Get metadata for images
    images.read(4)  # skip the magic_number
    number_of_images = images.read(4)
    number_of_images = unpack('>I', number_of_images)[0]
    rows = images.read(4)
    rows = unpack('>I', rows)[0]
    cols = images.read(4)
    cols = unpack('>I', cols)[0]

    # Get metadata for labels
    labels.read(4)  # skip the magic_number
    N = labels.read(4)
    N = unpack('>I', N)[0]

    if number_of_images != N:
        raise Exception('number of labels did not match the number of images')

    # 2D dimension
    rowscols = rows * cols

    # Get the data
    x = zeros((N, rowscols), dtype=np.float32)  # Initialize numpy array
    y = zeros((N), dtype=np.int32)  # Initialize numpy array
    for i in range(N):
        if i % 1000 == 0:
            print("i: %i" % i)
        for row in range(rowscols):
            # for col in range(cols):
            tmp_pixel = images.read(1)  # Just a single byte
            tmp_pixel = unpack('>B', tmp_pixel)[0]
            x[i][row] = tmp_pixel
        tmp_label = labels.read(1)
        y[i] = unpack('>B', tmp_label)[0]
    return (x, y)


def compute(train_x, train_y, test_x, test_y):
    compute_start_time = timeit.default_timer()
    n_features = train_x.shape[1]
    n_classes = 10

    batch_size = 50
    n_batches = train_x.shape[0] / batch_size

    n_epochs = 5

    std_train_x_float = ((train_x - np.mean(train_x)) / np.std(train_x)).astype(theano.config.floatX)
    train_y_int = train_y.astype(np.int32)

    std_test_x_float = ((test_x - np.mean(test_x)) / np.std(test_x)).astype(theano.config.floatX)
    test_y_int = test_y.astype(np.int32)

    indices = np.arange(std_train_x_float.shape[0])
    np.random.shuffle(indices)
    std_train_x_float = std_train_x_float[indices]
    train_y_int = train_y_int[indices]

    print std_train_x_float.dtype
    print std_train_x_float.shape

    print train_y_int.dtype
    print train_y_int.shape

    classifier = LogisticRegression(n_features, n_classes, learning_rate=0.01)

    for epoch in xrange(n_epochs):
        e_err = 0.
        for bIdx in xrange(n_batches):
            batch_x = std_train_x_float[bIdx * batch_size:(bIdx + 1) * batch_size]
            batch_y = train_y_int[bIdx * batch_size:(bIdx + 1) * batch_size]
            err = classifier.train(batch_x, batch_y)
            e_err += err
        print "Epoch: %d, Negative Log-Likelihood (mean over training set): %.4f" % (epoch, e_err / n_batches)

    n_predictionErrors = classifier.evaluate(std_test_x_float, test_y_int)
    print "0/1 Loss on Testset (Number of Mistakes): %d / %d" % (n_predictionErrors, test_y_int.shape[0])

    compute_end_time = timeit.default_timer()
    print ('Compute data ran for %.1fs' % ((compute_end_time - compute_start_time)))


def load_data(train_image, train_label, test_image, test_label):
    load_start_time = timeit.default_timer()
    (train_x, train_y) = get_labeled_data(train_image, train_label)
    (test_x, test_y) = get_labeled_data(test_image, test_label)
    load_end_time = timeit.default_timer()
    print ('Load data ran for %.1fs' % ((load_end_time - load_start_time)))

    return train_x, train_y, test_x, test_y


if __name__ == "__main__":
    print ("Start time recording")
    tot_start_time = timeit.default_timer()

    nb_files = 15

    for i in range(nb_files):
        (train_x, train_y, test_x, test_y) = load_data("new_mnist/%d_train_patterns" % i,
                                                       "new_mnist/%d_train_labels" % i,
                                                       "new_mnist/%d_test_patterns" % i, "new_mnist/%d_test_labels" % i)
        compute(train_x, train_y, test_x, test_y)

    tot_end_time = timeit.default_timer()
    print ('Global application ran for %.1fs' % ((tot_end_time - tot_start_time)))
