import timeit
from struct import unpack
from numpy import zeros
import numpy as np
from Logger import create_logger

class Reader(object):
    def __init__(self, imagefile, labelfile, queue_in, q_time, rowscols, list_indices):
        self.imagefile = imagefile
        self.labelfile = labelfile
        self.queue_in = queue_in
        # self.q_max_size = q_max_size
        self.rowscols = rowscols
        self.list_indices = list_indices
        self.q_time = q_time
        # self.logger = create_logger('Reader.log')


    def get_labeled_data(self):
        """Read input-vector (image) and target class (label, 0-9) and return
           it as list of tuples.
        """
        # Open the images with gzip in read binary mode
        # images = gzip.open(imagefile, 'rb')
        # labels = gzip.open(labelfile, 'rb')

        lab_start_time = timeit.default_timer()

        # Camille : No gzip in our use case
        images = open(self.imagefile, 'rb')
        labels = open(self.labelfile, 'rb')

        # Read the binary data

        # We have to get big endian unsigned int. So we need '>I'

        # Get metadata for images
        images.read(4)  # skip the magic_number
        number_of_images = images.read(4)
        number_of_images = unpack('>I', number_of_images)[0]
        rows = images.read(4)
        rows = unpack('>I', rows)[0]
        cols = images.read(4)
        cols = unpack('>I', cols)[0]

        # Get metadata for labels
        labels.read(4)  # skip the magic_number
        N = labels.read(4)
        N = unpack('>I', N)[0]

        if number_of_images != N:
            raise Exception('number of labels did not match the number of images')

        # 2D dimension
        rowscols = rows * cols
        print rowscols

        # Get the data
        for i in range(N):
            x = zeros((1, rowscols), dtype=np.float32)  # Initialize numpy array
            if i % 1000 == 0:
                print("i: %i" % i)
            for row in range(rowscols):
                # for col in range(cols):
                tmp_pixel = images.read(1)  # Just a single byte
                tmp_pixel = unpack('>B', tmp_pixel)[0]
                x[0][row] = tmp_pixel
            # print ("debug")
            y = zeros((1), dtype=np.int32)  # Initialize numpy array
            tmp_label = labels.read(1)
            y = unpack('>B', tmp_label)[0]
            self.queue_in.put([x, y])
        print ('\033[92m')
        print ("size input queue")
        print (self.queue_in.qsize())
        print ('\033[0m')
        lab_end_time = timeit.default_timer()
        print ('Get labeled data ran for %.1fs' % ((lab_end_time - lab_start_time)))

    def start(self):
        startR_start_time = timeit.default_timer()
        j = 0
        for i in self.list_indices:
            try:
                j = i
                self.read(j, self.rowscols)
            except:
                j = j - 1
        startR_end_time = timeit.default_timer()
        # print ('Start Reader ran for %.1fs' % ((startR_end_time - startR_start_time)))

    def read(self, i, rowscols):
        read_start_time = timeit.default_timer()
        images = open(self.imagefile, 'rb')
        labels = open(self.labelfile, 'rb')
        x = zeros((1, rowscols), dtype=np.float32)  # Initialize numpy array
        start = 16 + (i * rowscols)
        images.seek(start)  # Look for the beginning
        # if i % 1000 == 0 or i % 1000 == 1:
            # print("read : %i" % i)
            # print ("queue input {}".format(self.queue_in.qsize()))
        for row in range(rowscols):
            tmp_pixel = images.read(1)  # Just a single byte
            tmp_pixel = unpack('>B', tmp_pixel)[0]
            x[0][row] = tmp_pixel
        y = zeros((1), dtype=np.int32)  # Initialize numpy array
        start_lab = 8 + i
        labels.seek(start_lab)
        tmp_label = labels.read(1)
        y = unpack('>B', tmp_label)[0]
        read2_end_time = timeit.default_timer()
        if self.queue_in.full():
            print ("READING QUEUE IS FULL")
        self.queue_in.put([x, y])
        read_end_time = timeit.default_timer()
        self.q_time.put((read2_end_time - read_start_time))
        # print ('Read ran for %.4fs' % ((read2_end_time - read_start_time)))
